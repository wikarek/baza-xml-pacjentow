<!DOCTYPE html> 
<html lag="pl">
    <head>
        <meta charset="UTF-8" />
        <title>Pacjenci</title>
        <meta name="description" content="Dane pacjentow szpitala">
        <meta name="keywords" content="Lekarze badanie">
        <meta name="author" content="Arek Wieclaw">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css" >
    </head>
    <body>
        <div id="main">
            <div id="menu">
            <h3>Tabela Pacjenci</h3>
                <table>
                    <tr>
                        <td>Id</td>
                        <td>Imie</td>
                        <td>Nazwisko</td>
                    </tr>
                    <?php 
                        $Pacjenci = simplexml_load_file('pacjent.xml');
                        foreach($Pacjenci->Pacjent as $pacjent) { ?>
                           <tr>
                                <td> <? echo $pacjent['id_p']; ?> </td>
                                <td> <? echo $pacjent['imie']; ?> </td>
                                <td> <? echo $pacjent['nazwisko']; ?> </td>
                           </tr>
                    <?php }?>                    
                </table>
            </div>
            <div id="content">
                <h3>Tabela Badan danego Pacjenta</h3>
                <table>
                    <tr>
                        <td>ID Badania</td>
                        <td>Nazwa Badania</td>
                        <td>Data Badania</td>
                        <td>Imie Lekarza</td>
                        <td>Nazwisko Lekarza</td>
                    </tr>
                    <?php foreach($Pacjenci->Pacjent as $pacjent) { ?>
                     <?php foreach($pacjent->badanie as $bad) { ?>
                        <tr>
                                <td> <? echo $bad['id']; ?> </td>
                                <td> <? echo $bad['nazwa']; ?> </td>
                                <td> <? echo $bad['data']; ?> </td>
                            <?php foreach($bad->lekarz as $lek) {?> 
                                <td> <? echo $lek->imie;?> </td>
                                <td> <? echo $lek->nazwisko;?> </td>
                            <?php }?>
                        </tr>
                     <?php }?>
                    <?php }?>
                </table>
            </div>
        </div>
    </body>
</html>