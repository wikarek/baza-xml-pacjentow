<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:stylesheet>

<xsl:template match="/">
<html>
    <title>Strona Szpitala</title>
<body>
    <xsl:apply-templates/>
</body>
</html>
</xsl:template>

<xsl:template match="Pacjenci">
    <h1>Tabela  Pacjentow</h1>
    <table>
        <tr>
            <td>ID</td>
            <td>Imie</td>
            <td>Nazwisko</td>
        </tr>
        <xsl:for-each  select="Pacjent" >
        <tr>
            <td> <xsl:value-of select="@id_p"/> </td>
            <td> <xsl:value-of select="@imie"/> </td>
            <td> <xsl:value-of select="@nazwisko"/> </td>
        </tr>
        </xsl:for-each>
    </table>      
</xsl:template>

<xsl:template match="lekarz">
    <h1>Tabela Lekarzy</h1>
    <table>
        <tr>
            <td>ID</td>
            <td>Imie</td>
            <td>Nazwisko</td>
            <td>Specjalność</td>
        </tr>
        <xsl:for-each  select="towar" >
        <tr>
            <td> <xsl:value-of select="id"/> </td>
            <td> <xsl:value-of select="imie"/> </td>
            <td> <xsl:value-of select="nazwisko"/> </td>
            <td> <xsl:value-of select="specjalnosc"/> </td>
        </tr>
        </xsl:for-each>
    </table>
</xsl:template>

<xsl:template match="Pacjent">
    <h1>Tabela Badan</h1>
    <table>
        <tr>
            <td>ID:</td>
            <td>nazwa:</td>
            <td>Data:</td>
        </tr>
        <xsl:for-each  select="badanie" >
        <tr>
            <td> <xsl:value-of select="@id"/> </td>
            <td> <xsl:value-of select="@nazwa"/> </td>
            <td> <xsl:value-of select="@data"/>  </td>
        </tr>
          </xsl:for-each>
    </table>
</xsl:template>

</xsl:stylesheet>